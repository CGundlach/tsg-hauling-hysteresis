return function(mod)
    mod.infopanel_setup = function()
        hauling_hysteresis_pallet_stocks_inspector_change_target = function( world, ref)
            local pallet1 = ecs_deref(world, ref)
            local stockc = ecs_get_component(pallet1, stock_component)
            local maxc = stockc.max_amount
            local minc = stockc.min_amount

            show_field_prompt( world, ref,
            _L("type_restock_at_stock", "Amount that should be aimed for during restock."),
            function(gp1)
                    tostring(gp1)
            end,
            function(gp1)
                local v = tonumber(gp1)
                if v then 
                    return round(v)
                else 
                    return false
                end,
            end,
            function(gp1, gp2) 
                if tonumber(gp2) == nil then 
                    return _L ("value_not_number" "Value is not a number")
                end
                if gp2 < 0 then
                    return _L("value_too_low" "Value too low")
                end
                if gp2 > maxc then
                    return _L("value_lower_max" "Value must be lower than max amount")
                end
                if gp2 < minc then
                    return _L("value_higher_min" "Value must be higher than min amount")
                end
                    return ""
            end,
            function(gp1)
                return ecs_get_component(gp1, hysteresis_stock_component).target_amount
            end,
            function(gp1, gp2) 
                local hysteresis_stockc = ecs_get_component(gp1, hysteresis_stock_component)
                local stockc = ecs_get_component(gp1, stock_component)
                hysteresis_stockc.target_amount = gp2
                ecs_mark_dirty(stockc)
            end)
        
        end

        hauling_hysteresis_pallet_stocks_inspector function(e)
            local world = e.world
            local stockc = ecs_get_component(e, stock_component)
            local ref = ecs_ref(e)
                return gui_observer({ 
                    subjects = list(stockc)
                    width = 240 
                    height = -1
                    callback = function() 
                        local sourcer_ref = stockc.deliver_from_object
                        local sourcer = ecs_deref(world sourcer_ref) 
                        return gui_v({ height = -1, width = 240 },
                            gui_h({ height = 45, width = 235 },
                                gui_h(
                                    { height = 40, width = 100 }, 
                                    gui_label({ align = "center", text = _L("resource", "Resource")})),
                                gui_button(
                                    { tag2 = bridge_hash("stock-panel-pick-resource"),
                                    tooltip = 
                                        _L("pick_resource_pallet", "Select the resource this object must keep in stock") ..
                                        "\n" ..
                                        _L("pick_resource_pallet_rb", "Right click to not stock any resource in this object")
                                    ),
                                    text = (..
                                        (if stockc.resource
                                            (.. "@" (.icons.px20 stockc.resource))
                                            "")
                                        " "
                                        (if stockc.resource
                                            (.name stockc.resource)
                                            _L("disabled" "Disabled"))
                                        " @ui_icon_20px_pen.png")
                                    .style "hiddenButton"
                                    .height 40 -- :bias '(5 0)
                                    .mouse_priority -10
                                    .callback fn(() picker_resource({
                                        .label _L("pick_resource_pallet" "Select the resource this object must keep in stock")
                                        .callback (fn (resource)
                                            (let (  e ecs_deref(world ref)
                                                stockc (and e ecs_get_component(e stock_component)) )
                                                (when stockc
                                                    core_station_pallet_set_resource(e resource)
                                                    bridge_changed_persisting(stockc)
                                                    ecs_mark_dirty(stockc))))
                                        .default stockc.resource
                                        .filterq stockc.filter
                                        .fixed_resources stockc.fixed_resources
                                        .allow_disabled true
                                        })
                                    )
                                    .callback_right_click (fn ()
                                        (locals
                                            e ecs_deref(world ref)
                                            stockc (and e ecs_get_component(e stock_component))
                                        )
                                        (when stockc
                                            core_station_pallet_set_resource(e nil)
                                            bridge_changed_persisting(stockc)
                                            ecs_mark_dirty(stockc)
                                        )
                                    )
                            }))
                            gui_h({ .height 45 .width 235 }
                                gui_h({ .height 40 .width 100 } gui_label({ .align "center" .text _L("restock_at" "Restock at") }))
                                (gui_button
                                    { .tooltip _L("type_restock_at_stock" "Restocking tasks will start when the stock amount is equal or lower to this value, until it reaches the max stock amount.")
                                    .text (.. tostring(stockc.min_amount) " @ui_icon_20px_pen.png")
                                    .style "hiddenButton"
                                    .height 40 -- :bias '(5 0)
                                    .mouse_priority -10
                                    .callback (fn () core_station_pallet_stocks_inspector_change_min(world ref)) }))
                            gui_h({ .height 45 .width 235 }
                                gui_h({ .height 40 .width 100 } gui_label({ .align "center" .text _L("max_stock" "Max stock") }))
                                (gui_button
                                    { .tooltip _L("type_new_max_stock" "Maximum stock amount to keep in this object")
                                    .text (.. tostring(stockc.max_amount) " @ui_icon_20px_pen.png")
                                    .style "hiddenButton"
                                    .height 40 -- :bias '(5 0)
                                    .mouse_priority -10
                                    .callback (fn () core_station_pallet_stocks_inspector_change_max(world ref)) }))

                            (if sourcer
                                (list
                                    gui_h({ .height 20 .width 235 }
                                        (gui_button
                                            { .text "@ui_icon_20px_lupa.png"
                                            .style "hiddenButton"
                                            .height 20
                                            .width 20
                                            .mouse_priority -10
                                            .callback (fn () (let ( s ecs_deref(world sourcer_ref) )
                                                (when s
                                                    ShellShowEntityAnywhere(s)))) })
                                        gui_v({ .width 5 })
                                        gui_label({ .align "center" .text
                                            format(false _L("deliver_from_source_at" "Source: ~A")
                                                ecs_name({ .e sourcer .default _L("object" "Object")})) }))
                                    gui_h({ .height 5 })
                                    (gui_button
                                        { .text _L("deliver_from_source_cancel" "Cancel exclusive source")
                                        .height 40 -- :bias '(5 0)
                                        .width 235
                                        .mouse_priority -10
                                        .callback (fn () (set stockc.deliver_from_object false)) })
                                    )
                            
                                (list
                                    gui_h({ .height 45 .width 235 }
                                        gui_label({ .align "center" .text _L("valid_sources" "Valid sources:") }))

                                    gui_h({ .height 45 .width 235 }
                                        gui_h({ .height 40 .width 100 } gui_label({ .align "center"  .text _L("deliver_from_storage" "Pallets") }))
                                        (widget_buttons_radio {
                                            .height 40
                                            .width 80
                                            .default (if stockc.deliver_from_storage 0 1)
                                            .labels list(_L("yes" "Yes") _L("no" "No"))
                                            .tooltips (list
                                                _L("deliver_from_storage_yes" "Haulers will deliver resources to this object from inside pallets")
                                                _L("deliver_from_storage_no" "Haulers will avoid delivering resources to this object when the source is a pallet")
                                            )
                                            .callback (fn (gp1) (let ( e ecs_deref(world ref) stockc (and e ecs_get_component(e stock_component)) )
                                                (when stockc (set stockc.deliver_from_storage (== gp1 0)))))}))
                                    gui_h({ .height 45 .width 235 }
                                        gui_h({ .height 40 .width 100 } gui_label({ .align "center" .text _L("deliver_from_ground" "Ground") }))
                                        (widget_buttons_radio {
                                            .height 40
                                            .width 80
                                            .default (if stockc.deliver_from_floor 0 1)
                                            .labels list(_L("yes" "Yes") _L("no" "No"))
                                            .tooltips (list
                                                _L("deliver_from_ground_yes" "Haulers will deliver resources to this object from resource stacks lying on the ground or inside a storage area")
                                                _L("deliver_from_ground_no" "Haulers will avoid delivering resources to this object when the source is a stack lying on the ground or inside a storage area")
                                            )
                                            .callback (fn (gp1) (let ( e ecs_deref(world ref) stockc (and e ecs_get_component(e stock_component)) )
                                                (when stockc (set stockc.deliver_from_floor (== gp1 0)))))}))

                                    gui_label({ .text _L("deliver_from_source" "Exclusive source") })
                                    gui_label({ .width 235 .text_size "small" .text _L("deliver_from_source_desc"
                                        "Right click on any pallet or storage area controller to set that object as the exclusive source for delivery of resources to this object.") })

                                    ))


                            job_warning_display(e))
                    end
                    })
                
        end



    end
end