# Hauling Hysteresis - A mod for The Spatials: Galactology by Weird And Wry

This mod aims to improve hauling efficiency by introducing a hysteresis system for hauling tasks, in effect allowing haulers to "overfill" a stockpile, heavily inspired by the "Hauling Hysteresis" mod for RimWorld by Steam User Vendan.  
This happens by adding an additional configuration field, "Target Amount", which controls the desired amount of items in a stockpile, while "Max Amount" still controls the maximum amount of items allowed in a stockpile.  
By default, "Target Amount" is set to the vanilla "Max Amount" value, while "Max Amount" is now doubled.  
When this mod is active, officers carrying items to a stockpile will still attempt to fill a stockpile up to the maximum, but the stockpile will stop asking for more items as soon as the target amount is reached.  
This mainly fixes the issue that officers will perpetually top off a stockpile that is currently being drawn from since it wouldn't ever be completely full if a producing offers takes resources from it while another officers is bringing resources to fill it up correctly.

## Important Information

This mod is only compatible with game version 3.14 and upwards, which is as of 2020-07-11 in the experimental branch on Steam and not yet part of the main game.

## Getting started

Install the mod into your game folder  
Run The Spatials: Galactology  
Select "New Game"  
Switch to the "Mods" tab and enable this mod  
Switch back to the "Welcome!" tab and click on "Start Game"

Ingame, once you have built an object with a stockpile (e.g. a pallet or a food canteen), you can adjust the amount of resources that should be delivered to it with the "Target Amount" and "Max Amount" fields. Your officers should do the rest.

## Known Issues

None

## Compatibility

The global variables this mod introduces are using the hauling_hysteresis_* naming scheme.

This mod expands several global functions of the stock system:
    - core_station_make_station_pallet
    - core_station_pallet_set_resource
    - stock_update

This is achieved by saving the global functions locally and overriding the globals with a call to the saved local functions and executing this mod's code after that. If anothe code expands these functions in a similar way, there should be no conflicts. Check the source to see the exact implementation.

This mod also removes the default stocks_control infopanel and replaces it with the hauling_hysteresis_stocks_control infopanel, which includes one additional field to alter the hauling hysteresis target value.  

## Authors

- Christopher Gundlach - initial work - @CGundlach

## Acknowledgments

Thanks to BkBarbee on the Steam Discussions, who's written a [helpful modding introduction](https://steamcommunity.com/sharedfiles/filedetails/?id=1100574260) for this game and given me a few helpful pointers,  
and many thanks to Weird and Wry who have created a change to the game to make mods like this possible, only a day after asking for help on how to create a mod like this. You are awesome!
