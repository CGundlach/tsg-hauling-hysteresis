return function (mod)

    include_lua(mod, "hauling_hysteresis_core.lua")
    include_lisp(mod, "hauling_hysteresis_infopanel.lisp")

    mod.on_prepare_rules = function (rules)
        mod.hauling_hysteresis_setup(rules)
        mod.run_hauling_hysteresis_overrides()
        mod.setup_infopanel()
    end

    mod.on_new_game = function ()
    end

    mod.on_gui_ready = function ()
        local world = game.station.body.world
        mod.initial_world_variables(wordl)
        mod.run_hauling_hysteresis_migration(world)
    end

    mod.on_landed_planet = function (body)
    end

    mod.on_takeoff_planet = function (body)
    end
end
