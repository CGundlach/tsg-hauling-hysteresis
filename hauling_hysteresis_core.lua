return function (mod)

    local list = require("l2l.list")
    mod.hauling_hysteresis_version = 1
    
    mod.hauling_hysteresis_setup = function (rules)
        hauling_hysteresis_stock_component = bridge_hash("hauling_hysteresis_stock_component")
    end

    -- patch function for updating existing entities to include the hysteresis stock component
    local patch_entity_for_hauling_hysteresis = function(entity)
        -- no need to patch them if they already have the component
        if is_ecs_has_component(entity, hauling_hysteresis_stock_component) then
            return
        end

        local pallet_stock_component = ecs_get_component(entity, stock_component)

        -- we don't want to change the user's settings, so keep the hauling goal the same
        local target_amount = pallet_stock_component.max_amount
        -- update max_amount to allow haulers to "overfill"
        pallet_stock_component.max_amount = target_amount * 2
        -- and finally add the hysteresis component
        ecs_make_add_component(entity, hauling_hysteresis_stock_component, 
            "target_amount", target_amount)
        building_rebuild_register_field(entity, hauling_hysteresis_stock_component, "target_amount")
    end

    local check_and_patch_for_hauling_hysteresis = function(entity)
        if not is_ecs_has_component(entity, hauling_hysteresis_stock_component) then 
            patch_entity_for_hauling_hysteresis(entity)
        end
    end

    mod.run_hauling_hysteresis_overrides = function()
        -- save the original pallet creation function
        local core_station_make_station_pallet_vanilla = core_station_make_station_pallet
        -- override the original creation function, so calls for it will call the hysteresis function instead
        core_station_make_station_pallet = function (world, e, obj, x, y, a)
            -- call original function first, then we can make our changes to the entity
            local pallet_entity = core_station_make_station_pallet_vanilla(world, e, obj, x, y, a)

            patch_entity_for_hauling_hysteresis(pallet_entity)
            return pallet_entity
        end

        -- override for the set_resource function to include a check for the hysteresis target amount
        local core_station_pallet_set_resource_vanilla = core_station_pallet_set_resource 
        core_station_pallet_set_resource = function (e, resource)
            core_station_pallet_set_resource_vanilla(e,resource)
            
            check_and_patch_for_hauling_hysteresis(e)

            local stockc = ecs_get_component(e, stock_component)
            local hauling_hysteresis_stockc = ecs_get_component(e, hauling_hysteresis_stock_component)

            if hauling_hysteresis_stockc.target_amount > stockc.max_amount then
                hauling_hysteresis_stockc.target_amount = stockc.max_amount
            end
        end

        --override for stock_update to implement hysteresis
        local stock_update_vanilla = stock_update
        stock_update = function(e)
            stock_update_vanilla(e)
            local stockc = ecs_get_component(e, stock_component)

            -- all of this only needs to run if we're in a restock phase, otherwise it's above maximum anyways
            if stockc.restock_phase then 
                
                -- first repeat a few steps from the original stock_update to get the current resource count
                local stacks_on_entity = ecs_query({
                    world = e.world,
                    rect = ecs_pos_rect(e),
                    must = list(ItemComponent_Kind, PositionComponent_Kind),
                    exclude = list(ChildComponent_Kind)
                })

                check_and_patch_for_hauling_hysteresis(e)
                
                local current = 0
                local effective target = 0

                local hauling_hysteresis_stockc = ecs_get_component(e, hauling_hysteresis_stock_component)

                if stockc.resource then
                    effective_target = hauling_hysteresis_stockc.target_amount
                end

                local resource_stacks_to_count = list()
                for key,entity in ipairs(stacks_on_entity) do
                    if stockc.resource and is_ecs_has_component(entity, stockc.resource.hashed) then
                        current = current + ecs_get_component(entity, ItemComponent_Kind).amount
                    end
                end

                if current >= effective_target then
                    ecs_try_remove_component(e, stock_required_component)
                    stockc.restock_phase = false
                end
            end
        end
    end

    mod.initialize_world_variables = function(world)
        if world.hauling_hysteresis == nil then
            world.hauling_hysteresis = {}
        end

        if world.hauling_hysteresis.migration_done == nil then
            world.hauling_hysteresis.migration_done = false
        end

        if world.hauling_hysteresis.version == nil then
            world.hauling_hysteresis.version = mod.hauling_hysteresis_version
        else
            if world.hauling_hysteresis.version < mod.hauling_hysteresis_version then
                world.hauling_hysteresis.migration_done = false
            end
        end
    end

    mod.run_hauling_hysteresis_migration = function(world)
        if world.hauling_hysteresis.migration_done == true then
            return
        end

        local stock_objects = ecs_query({
            world = world,
            must = list(stock_component),
            exclude = list(hauling_hysteresis_stock_component)
        })

        for key,entity in ipairs(stock_objects) do 
            patch_entity_for_hauling_hysteresis(entity)
        end

        world.hauling_hysteresis.migration_done = true
        world.hauling_hysteresis.version = mod.hauling_hysteresis.version
    end
end
    