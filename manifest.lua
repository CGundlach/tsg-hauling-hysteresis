return {
    name = "Hauling Hysteresis",
    description = "Introduces a hauling hysteresis system, which aims to improve hauling efficiency by separating the maximum amount a stockpile can hold and the amount a hauler needs to fill for it to stop requesting new items.\n",
    -- This field is mandatory and for now it has to be 4.
    api_version = 4
}